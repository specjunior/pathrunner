using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	public static GameManager me { get; private set; }
	public int levelNumber = 0;
	public int nextLevelNumber = 0;

	[FoldoutGroup("Bindings"), SerializeField] private Transform playerStartPosition;
	[FoldoutGroup("Bindings"), SerializeField] private PlayerController player;
	[FoldoutGroup("Bindings"), SerializeField] private PauseScreen pauseScreen;
	[FoldoutGroup("Bindings"), SerializeField] private WinScreen winScreen;
	[FoldoutGroup("Bindings"), SerializeField] private Camera previewCamera;
	[FoldoutGroup("Bindings"), SerializeField] private Floor floor;
	[FoldoutGroup("Options"), SerializeField] private float playerReturnToStartSpeed = 5f;

	[HideInInspector] public UnityEvent OnPreviewCameraEnable;
	[HideInInspector] public UnityEvent OnPreviewCameraDisable;
	//[HideInInspector] public UnityEvent OnFinishGame;
	private List<Cannon> cannons = new List<Cannon>();

	private float startTime = 0;
	private float finishTime = 0;
	private bool isPaused = false;
	private int attempt = 0;

	//UNITY
	private void Awake()
	{
		if(me == null)
			me = this;

		OnPreviewCameraEnable.AddListener(() => previewCamera.enabled = true);
		OnPreviewCameraDisable.AddListener(() => previewCamera.enabled = false);
	}

	private IEnumerator Start()
	{
		player.DisableLook();
		player.DisableMovement();
		player.DisableCamera();
		DeactivateCannons();

		yield return floor.ShowPreviewPathAnim();
		yield return new WaitForSeconds(1f);
		yield return floor.ResetFloorAnim(0f);
		yield return new WaitForSeconds(0.5f);

		DisablePreviewCamera();
		ActivateCannons();
		HideCursor();

		player.EnableLook();
		player.EnableMovement();
		player.EnableCamera();

		startTime = Time.time;
		finishTime = Time.time;
	}

	//PUBLIC
	public void PlayerFailed()
	{
		StartCoroutine(OnPlayerFailed());
	}

	public void PlayerFinished()
	{
		StartCoroutine(OnPlayerFinished());
	}

	public void RegisterCannon(Cannon cannon)
	{
		cannons.Add(cannon);
	}

	public void Pause()
	{
		Debug.Log("Pause");
		if(isPaused)
			Unpause_();
		else
		{
			isPaused = true;
			ShowCursor();
			Time.timeScale = 0f;
			pauseScreen.Open();
		}
	}

	public void Unpause_()
	{
		isPaused = false;
		Time.timeScale = 1f;
		HideCursor();
		pauseScreen.Close();
	}

	public void LoadMainMenu_()
	{
		ShowCursor();
		Time.timeScale = 1f;
		SceneManager.LoadScene("MainMenu");
	}

	public void LoadNextLevel_()
	{
		if(nextLevelNumber == -1)
			LoadMainMenu_();
		Time.timeScale = 1f;
		SceneManager.LoadScene($"Level {nextLevelNumber}");
	}

	public float LevelTime() => finishTime - startTime;

	//PRIVATE

	private void EnablePreviewCamera()
	{
		OnPreviewCameraEnable.Invoke();
	}

	private void DisablePreviewCamera()
	{
		OnPreviewCameraDisable.Invoke();
	}

	private void ActivateCannons()
	{
		foreach(Cannon cannon in cannons)
			cannon.Active = true;
	}

	private void DeactivateCannons()
	{
		foreach(Cannon cannon in cannons)
			cannon.Active = false;
	}

	private void ShowCursor()
	{
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;
	}

	private void HideCursor()
	{
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
	}

	//ENUMERATORS

	private IEnumerator OnPlayerFinished()
	{
		finishTime = Time.time;
		floor.Active = false;

		player.DisableLook();
		player.DisableMovement();
		winScreen.Open();
		ShowCursor();

		yield return null;
	}

	private IEnumerator OnPlayerFailed()
	{
		player.DisableMovement();
		DeactivateCannons();

		if(attempt > 0 && attempt % 3 == 0)
		{
			yield return new WaitForSeconds(1f);
			yield return ReturnPlayerCoroutine();
			EnablePreviewCamera();
			yield return floor.ShowPreviewPathAnim();
			yield return floor.ResetFloorAnim(0f);
			DisablePreviewCamera();
		}
		else
		{
			yield return new WaitForSeconds(1f);
			yield return ReturnPlayerCoroutine();
			yield return floor.ResetFloorAnim(0f);
		}
		ActivateCannons();
		player.EnableMovement();
		attempt++;
	}

	private IEnumerator ReturnPlayerCoroutine()
	{
		float startDistance = Vector3.Distance(player.transform.position, playerStartPosition.position);
		float distance = startDistance;

		while(distance > 0.001f)
		{
			player.transform.position = Vector3.Lerp(player.transform.position, playerStartPosition.position, playerReturnToStartSpeed * Time.deltaTime);
			distance = Vector3.Distance(player.transform.position, playerStartPosition.position);

			yield return null;
		}
	}

	private void OnDestroy()
	{
		me = null;
	}
}