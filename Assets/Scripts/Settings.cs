using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Settings
{
	public static float mainVolume = 1f;
	public static float musicVolume = 1f;
	public static float soundVolume = 1f;
	public static float voiceVolume = 1f;

	public static float mouseSensitivityX = 2f;
	public static float mouseSensitivityY = 2f;
	public static bool invertedMouseX = false;
	public static bool invertedMouseY = false;

	public static Vector2 Sensitivity
		=> new Vector2(
			invertedMouseX ? -mouseSensitivityX : mouseSensitivityX,
			invertedMouseY ? -mouseSensitivityY : mouseSensitivityY
		);

	public static bool subtitles = true;
}