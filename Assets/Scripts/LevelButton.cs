using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
	[SerializeField] private Button button;
	private MainMenu mainMenu;
	private int levelNumber = 0;

	public void Configure(MainMenu mainMenu_, int levelNumber_)
	{
		mainMenu = mainMenu_;
		levelNumber = levelNumber_;
		GetComponentInChildren<Text>().text = $"Level {levelNumber}";
		button.onClick.AddListener(() => mainMenu.PlayLevel(levelNumber));
	}
}