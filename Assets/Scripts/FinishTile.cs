using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishTile : MonoBehaviour
{
	[SerializeField] private Floor floor;

	private void OnTriggerEnter(Collider other)
	{
		if(other.transform.tag == "Player")
		{
			floor.PlayerEnteredFinishTile();
		}
	}
}