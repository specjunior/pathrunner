using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraLook : MonoBehaviour
{
	private static CameraLook me = null;

	public bool Lock = false;

	public float Pitch
	{
		get => ((transform.localEulerAngles.x >= 180 ?
			transform.localEulerAngles.x : transform.localEulerAngles.x + 360
		) - 360);
		set => transform.localEulerAngles = new Vector3(
			Mathf.Clamp(value, clampInDegrees.x, clampInDegrees.y),
			transform.localEulerAngles.y, transform.localEulerAngles.z
		);
	}

	#region variables

	[FoldoutGroup("Movement"), SerializeField]
	private Vector2 clampInDegrees = new Vector2(-89, 89);
	[FoldoutGroup("Movement"), SerializeField]
	private float constSensitivityMultipler = 100;
	[BoxGroup("Movement/DEBUG"), SerializeField, ReadOnly]
	private Vector2 sensitivity = new Vector2(1, 1);
	[Space]
	private Vector2 mouseDelta;

	[SerializeField, FoldoutGroup("Bindings"), Required] private Transform body;
	[SerializeField, FoldoutGroup("Bindings")] private InputControl input;
	[SerializeField, FoldoutGroup("Bindings"), Required] private CharacterMovement movement;
	[SerializeField, FoldoutGroup("Bindings"), Required] private new Camera camera;

	//Poruszanie
	[SerializeField, FoldoutGroup("Bobbing values")]
	private float bobbingSpeedWalk = 17f;
	[SerializeField, FoldoutGroup("Bobbing values")]
	private float bobbingSpeedSprint = 24f;
	[SerializeField, FoldoutGroup("Bobbing values")]
	private float bobbingAmountWalk = 0.07f;
	[SerializeField, FoldoutGroup("Bobbing values")]
	private float bobbingAmountSprint = 0.09f;
	[SerializeField, FoldoutGroup("Bobbing values")]
	private float leftRightAngle = 5f;
	[SerializeField, FoldoutGroup("Bobbing values")]
	private float cameraStrafeRotationSpeed = 30f;

	private Vector2 absRawMove;
	private Vector3 startCameraPosition;
	private float timer = 0;
	private float bobbingAmount = 0.07f;
	private float bobbingSpeed = 0.07f;
	private float multipler = 1;

	#endregion variables

	private void Awake()
	{
		me = this;
	}

	private void OnDestroy()
	{
		me = null;
	}

	private void Start()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		startCameraPosition = transform.localPosition;
	}

	private void Update()
	{
		if(Lock)
			return;

		AbsMove();

		var tempSensitivity = Settings.Sensitivity * Time.deltaTime * constSensitivityMultipler;

		//CameraBobbingUpDown();
		mouseDelta.Set(input.Look.x, input.Look.y);

		var angle = Pitch;
		Pitch = angle + (-mouseDelta.y * tempSensitivity.y);

		//CameraBobbingLeftRight();

		if(body)
			body.transform.Rotate(0, mouseDelta.x * tempSensitivity.x, 0);
		else
			transform.Rotate(0, mouseDelta.x * tempSensitivity.x, 0);
	}

	private void AbsMove()
	{
		absRawMove.Set(
			Mathf.Abs(input.MoveRaw.x), Mathf.Abs(input.MoveRaw.y)
		);
	}

	private void CameraBobbingUpDown()
	{
		if(Lock || movement.Lock)
			return;

		if(movement.Sprint)

		{
			bobbingAmount = bobbingAmountSprint;
			bobbingSpeed = bobbingSpeedSprint;
		}
		else
		{
			bobbingAmount = bobbingAmountWalk;
			bobbingSpeed = bobbingSpeedWalk;
		}

		multipler = Mathf.Max(absRawMove.x, absRawMove.y);
		bobbingSpeed *= multipler;

		if(input.Move.y != 0)
		{
			timer += Time.deltaTime * bobbingSpeed;
			transform.localPosition = new Vector3(
				transform.localPosition.x,
				startCameraPosition.y + Mathf.Sin(timer) * bobbingAmount,
				transform.localPosition.z
			);
		}
		else
		{
			timer = 0;
			transform.localPosition = new Vector3(
				transform.localPosition.x,
				Ext.Lerp(
					transform.localPosition.y,
					startCameraPosition.y,
					Time.deltaTime * bobbingSpeed
				),
				transform.localPosition.z
			);
		}
	}

	private void CameraBobbingLeftRight()
	{
		if(Lock || movement.Lock)
			return;

		Vector3 temp = transform.localRotation.eulerAngles;

		if(input.Move.x != 0)
			temp.z = -leftRightAngle * input.Move.x;
		else
			temp.z = 0;

		transform.localRotation = Quaternion.Lerp(
			transform.localRotation, Quaternion.Euler(temp),
			cameraStrafeRotationSpeed * 0.5f * Time.deltaTime
		);
	}
}