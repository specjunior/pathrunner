using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public CharacterMovement Movement => movement;
	public CameraLook Look => look;

	[SerializeField] private Camera cam;
	[SerializeField] private CharacterMovement movement;
	[SerializeField] private CameraLook look;

	public void EnableCamera() => cam.enabled = true;

	public void EnableLook() => look.Lock = false;

	public void DisableCamera() => cam.enabled = false;

	public void DisableLook() => look.Lock = true;

	public void EnableMovement() => movement.Lock = false;

	public void DisableMovement() => movement.Lock = true;
}