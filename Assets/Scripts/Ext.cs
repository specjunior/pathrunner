using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static partial class Ext
{
	public static float Proc2Db(float value)
	{
		value = Ext.Clamp01(value);
		if(value <= 0)
			return -80;
		return 20.0f * Mathf.Log10(value);
	}

	public static float Db2Proc(float dB)
		=> Mathf.Pow(10.0f, dB / 20.0f);

	public static float Clamp(float val, float min, float max)
		=> val < min ? min : (val > max ? max : val);

	public static float Clamp01(float val)
		=> val < 0 ? 0 : (val > 1 ? 1 : val);

	public static int Clamp(int val, int min, int max)
		=> val < min ? min : (val > max ? max : val);

	public static float Lerp(float a, float b, float t) => (1 - t) * a + t * b;

	public static float Min(float a, float b) => a < b ? a : b;

	public static float Max(float a, float b) => a > b ? a : b;

	public static int Min(int a, int b) => a < b ? a : b;

	public static int Max(int a, int b) => a > b ? a : b;

	public static void Enable(this CanvasGroup value)
	{
		value.alpha = 1f;
		value.blocksRaycasts = true;
		value.interactable = true;
	}

	public static void Disable(this CanvasGroup value)
	{
		value.alpha = 0f;
		value.blocksRaycasts = false;
		value.interactable = false;
	}

	public static T RandomElement<T>(this IEnumerable<T> str)
	{
		int count = str.Count();
		if(count == 0)
			return default(T);
		return str.ElementAt(UnityEngine.Random.Range(0, count));
	}
}