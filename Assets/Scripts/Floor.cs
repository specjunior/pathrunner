using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public partial class Floor : MonoBehaviour
{
	public bool Active
	{
		get { return active; }
		set { active = value; }
	}

	[SerializeField, FoldoutGroup("Prefabs")] private GameObject tile;
	[SerializeField, FoldoutGroup("Prefabs")] private GameObject stairs;
	[SerializeField, FoldoutGroup("Prefabs")] private GameObject cube;

	[FoldoutGroup("Path"), SerializeField]
	private List<FloorElement> path = new List<FloorElement>();
	private List<FloorElement> indicatedElements = new List<FloorElement>();

	private bool active = true;
	private FloorElement[] allChildren;

	private void Start()
	{
		allChildren = GetComponentsInChildren<FloorElement>(true);
	}

	public void PlayerEnteredFinishTile()
	{
		if(!active) return;
		if(indicatedElements.Count == path.Count)
			GameManager.me.PlayerFinished();
		else
		{
			active = false;
			GameManager.me.PlayerFailed();
		}
	}

	public void PlayerEnteredFloorElement(FloorElement floorElement)
	{
		if(!active) return;

		indicatedElements.Add(floorElement);

		if(indicatedElements.Count <= path.Count && path[indicatedElements.Count - 1].Equals(indicatedElements.Last()))
			floorElement.GoodAnswer();
		else
		{
			floorElement.BadAnswer();
			active = false;
			GameManager.me.PlayerFailed();
			return;
		}
	}

	public IEnumerator ShowPreviewPathAnim()
	{
		float timeForOne = 4.0f / path.Count;
		yield return new WaitForSeconds(1);
		foreach(FloorElement child in path)
		{
			child.ShowPreview();
			indicatedElements.Add(child);
			yield return new WaitForSeconds(timeForOne);
		}
	}

	public IEnumerator ResetFloorAnim(float animationTime)
	{
		float timeForOneElement = animationTime / allChildren.Length;

		foreach(FloorElement child in allChildren)
		{
			child.Restart();
			yield return new WaitForSeconds(timeForOneElement);
		}
		indicatedElements.Clear();
		active = true;
	}
}

public partial class Floor : MonoBehaviour
{
#if UNITY_EDITOR

	[Button, FoldoutGroup("Path")]
	private void AddSelectedToPath()
	{
		FloorElement selected = ((GameObject)Selection.activeObject).GetComponent<FloorElement>();
		if(selected)
			path.Add(selected);
	}

	[Button, FoldoutGroup("Adding elements")]
	private void AddTile()
	{
		GameObject temp = (GameObject)PrefabUtility.InstantiatePrefab(tile, transform);
		temp.transform.position = transform.position;
		temp.GetComponent<FloorElement>().Configure(this);
		Selection.objects = new Object[] { temp };
	}

	[Button, FoldoutGroup("Adding elements")]
	private void AddCube()
	{
		GameObject temp = (GameObject)PrefabUtility.InstantiatePrefab(cube, transform);
		temp.transform.position = transform.position;
		temp.GetComponent<FloorElement>().Configure(this);
		Selection.objects = new Object[] { temp };
	}

	[Button, FoldoutGroup("Adding elements")]
	private void AddStairs()
	{
		GameObject temp = (GameObject)PrefabUtility.InstantiatePrefab(stairs, transform);
		temp.transform.position = transform.position;
		temp.GetComponent<FloorElement>().Configure(this);
		Selection.objects = new Object[] { temp };
	}

	[Button, FoldoutGroup("Tools")]
	private void AlignToGrid(bool x = true, bool y = false, bool z = true)
	{
		Transform[] allChildren = GetComponentsInChildren<Transform>(true);

		foreach(Transform obj in allChildren)
		{
			if(obj.Equals(transform)) continue;
			obj.transform.localPosition = new Vector3(
				x ? Mathf.Round(obj.transform.localPosition.x) : obj.transform.localPosition.x,
				y ? Mathf.Round(obj.transform.localPosition.y) : obj.transform.localPosition.y,
				z ? Mathf.Round(obj.transform.localPosition.z) : obj.transform.localPosition.z
			);
		}
	}

	[Button, FoldoutGroup("Tools")]
	private void CreateFloorOfSize(int x = 5, int y = 5, bool removeChildren = false)
	{
		if(removeChildren)
		{
			Transform[] allChildren = GetComponentsInChildren<Transform>(true);
			foreach(Transform obj in allChildren)
			{
				if(obj.Equals(transform)) continue;
				GameObject.DestroyImmediate(obj.gameObject);
			}
		}

		for(int i = 0; i < x; i++)
		{
			for(int j = 0; j < y; j++)
			{
				GameObject temp = (GameObject)PrefabUtility.InstantiatePrefab(tile, transform);
				temp.transform.position = transform.position + new Vector3(i * temp.transform.localScale.x, 0, j * temp.transform.localScale.z);
				temp.GetComponent<FloorElement>().Configure(this);
			}
		}
	}

	private static void DrawString(string text, Vector3 worldPos, Color? colour = null)
	{
		UnityEditor.Handles.BeginGUI();

		Color restoreColor = GUI.color;
		if(colour.HasValue) GUI.color = colour.Value;
		var view = UnityEditor.SceneView.currentDrawingSceneView;
		Vector3 screenPos = view.camera.WorldToScreenPoint(worldPos);

		if(screenPos.y < 0 || screenPos.y > Screen.height || screenPos.x < 0 || screenPos.x > Screen.width || screenPos.z < 0)
		{
			GUI.color = restoreColor;
			UnityEditor.Handles.EndGUI();
			return;
		}

		Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
		GUI.Label(new Rect(
			screenPos.x - (size.x / 2),
			-screenPos.y + view.position.height - 10,
			size.x,
			size.y
			), text);
		GUI.color = restoreColor;

		UnityEditor.Handles.EndGUI();
	}

	private void OnDrawGizmos()
	{
		if(path.Count < 1) return;
		Gizmos.color = Color.red;
		for(int i = 0; i < path.Count - 1; i++)
		{
			if(path[i] == null) continue;
			Gizmos.color = Color.red;
			Gizmos.DrawCube(path[i].transform.position, Vector3.one / 2);
			Gizmos.color = Color.yellow;
			Gizmos.DrawCube(path[i].transform.position + (path[i + 1].transform.position - path[i].transform.position) / 2, Vector3.one / 4);

			DrawString(i.ToString(), path[i].transform.position, Color.green);
		}
		Gizmos.color = Color.cyan;
		Gizmos.DrawCube(path[path.Count - 1].transform.position, Vector3.one / 2);
		DrawString("END".ToString(), path[path.Count - 1].transform.position, Color.green);
	}

#endif
}