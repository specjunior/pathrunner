using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	[SerializeField] private GameObject explosion;
	[SerializeField] private int hitsOnFloorToExplode = 4;

	private int counter = 0;

	private void OnCollisionEnter(Collision collision)
	{
		if(collision.transform.tag == "Player")
		{
			collision.transform.GetComponent<PlayerController>().Movement.AddPushForce
				((collision.transform.position - transform.position).normalized * 20f);
			Instantiate(explosion, transform.position, Quaternion.identity);
			Destroy(this.gameObject);
			return;
		}

		counter++;
		if(counter >= hitsOnFloorToExplode)
		{
			Instantiate(explosion, transform.position, Quaternion.identity);
			Destroy(this.gameObject);
		}
	}
}