using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
	public bool Active
	{ get { return active; } set { timer = 0; active = value; } }

	[FoldoutGroup("Bindings"), SerializeField] private Transform shootPoint;
	[FoldoutGroup("Bindings"), SerializeField] private GameObject explosion;
	[FoldoutGroup("Bindings"), SerializeField] private GameObject bullet;
	[FoldoutGroup("Bindings"), SerializeField] private AudioSource audioSource;
	[FoldoutGroup("Bindings"), SerializeField] private AudioClip shootClip;
	[FoldoutGroup("Options"), SerializeField] private float shootInterval = 2.0f;
	[FoldoutGroup("Options"), SerializeField] private float shootForce = 5.0f;

	private bool active = true;
	private float timer = 0;
	private Vector3 shootDirection;

	private void Awake()
	{
		GameManager.me.RegisterCannon(this);
	}

	private void Update()
	{
		if(!Active) return;

		if(timer < shootInterval)
		{
			timer += Time.deltaTime;
			return;
		}
		Shoot();
		timer = 0f;
	}

	[Button]
	private void Shoot()
	{
		Instantiate(explosion, shootPoint.position, Quaternion.identity);
		shootDirection = (shootPoint.position - transform.position).normalized;
		GameObject go = Instantiate(bullet, shootPoint.position, Quaternion.identity);
		go.GetComponent<Rigidbody>().AddForce(shootDirection * shootForce, ForceMode.Impulse);
		audioSource.PlayOneShot(shootClip);
	}
}