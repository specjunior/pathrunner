using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputControl : MonoBehaviour
{
	//ACCESSORS
	public Vector2 Move => controls.Player.Move.ReadValue<Vector2>().normalized;

	public Vector2 MoveRaw => controls.Player.Move.ReadValue<Vector2>();

	public Vector2 Look => new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

	public bool Sprint => controls.Player.Sprint.IsPressed();

	public bool Jump => controls.Player.Jump.IsPressed();

	private Controls controls;

	private void Start()
	{
		controls = new Controls();
		controls.Enable();

		controls.UI.Pause.performed += ctx => GameManager.me.Pause();
	}

	private void OnDisable()
	{
		controls.Disable();
	}
}