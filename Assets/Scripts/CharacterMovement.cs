using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
	public bool Lock = false;

	public bool Sprint { get; set; }
	public bool Jump { get; set; }
	[BoxGroup("DEBUG"), ShowInInspector, ReadOnly]
	public float JumpProcess { get; set; }

	[BoxGroup("DEBUG"), ShowInInspector, ReadOnly]
	public bool Grounded
	{
		get => controller.isGrounded; set { }
	}

	public Vector3 ExternalForce;

	[BoxGroup("Bindings"), SerializeField, Required] private InputControl input;
	[BoxGroup("Bindings"), SerializeField, Required] private CharacterController controller = null;

	[BoxGroup("Movement"), SerializeField] private float walkSpeed = 4.0f;
	[BoxGroup("Movement"), SerializeField] private float runSpeed = 8.0f;
	[BoxGroup("Movement/Jump"), SerializeField] private float jumpForce = 2f;
	[BoxGroup("Movement/Jump"), SerializeField] private AnimationCurve jumpCurve = new AnimationCurve();

	[BoxGroup("Audio"), Required] public AudioSource audioSource = null;
	[BoxGroup("Audio/Clips"), Required] public AudioClip jumpClip = null;
	[BoxGroup("Audio/Clips"), Required] public AudioClip[] soundStep = new AudioClip[8];
	[BoxGroup("Audio/Clips/Settings"), SerializeField] private float stepDuration = 0.45f;
	[BoxGroup("Audio/Clips/Settings"), SerializeField] private float stepSprintDuration = 0.3f;

	private Vector3 moveDir = Vector3.zero;
	private Vector3 pushForce;

	private Vector2 move;
	private bool jump;
	private bool jumpFromPreviousFrame;
	private float currentStepDuration = 0f;

	private void Start()
	{
		controller.enabled = true;
	}

	private void FixedUpdate()
	{
		move = input.Move;
		Sprint = input.Sprint;

		jumpFromPreviousFrame = jump;
		jump = input.Jump;
		Jump = !jumpFromPreviousFrame && jump;
	}

	private void Update()
	{
		float speed = walkSpeed;
		if(Sprint)
			speed = runSpeed;

		SetMoveDirection();

		Vector3 moveDirection = speed * (
			transform.forward * moveDir.z
			+ transform.right * moveDir.x
			+ transform.up * moveDir.y
		).normalized;

		ExternalForce = Vector3.Lerp(
				ExternalForce, Physics.gravity, Time.deltaTime
			);

		if(JumpProcess > 0f)
		{
			moveDirection -= Physics.gravity * jumpCurve.Evaluate(
				JumpProcess
			) * jumpForce;
		}

		JumpProcess = Ext.Lerp(JumpProcess, 0f, Time.deltaTime);

		if(input.Jump)
			if(Grounded)
			{
				JumpProcess = 1f;
				audioSource.PlayOneShot(jumpClip);
			}

		pushForce = Vector3.Lerp(
				pushForce, new Vector3(Physics.gravity.x, 0, Physics.gravity.z), Time.deltaTime
			);

		if(!Lock)
		{
			if(Grounded)
				Step();
			controller.Move(
				(moveDirection + ExternalForce + pushForce) * Time.deltaTime
			);
		}
	}

	public void AddPushForce(Vector3 force)
	{
		pushForce = force;
	}

	public void SetMoveDirection()
	{
		moveDir = Lock ? Vector3.zero : new Vector3(move.x, 0, move.y);
	}

	private void Step()
	{
		if(Mathf.Abs(moveDir.z) > 0.01f
			|| Mathf.Abs(moveDir.x) > 0.01f
			)
		{
			currentStepDuration -= Time.deltaTime;
			if(currentStepDuration <= 0f)
			{
				audioSource.PlayOneShot(soundStep.RandomElement());
				currentStepDuration = Sprint ?
					stepSprintDuration : stepDuration;
			}
		}
	}
}