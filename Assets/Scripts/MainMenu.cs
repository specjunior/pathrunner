using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
	[SerializeField] private int levelsCount;
	[SerializeField] private GameObject levelButton;
	[SerializeField] private Transform levelButtonsParent;

	private void Start()
	{
		for(int i = 0; i < levelsCount; i++)
		{
			GameObject go = Instantiate(levelButton, levelButtonsParent);
			go.GetComponent<LevelButton>().Configure(this, i);
		}
	}

	public void Play()
	{
		SceneManager.LoadScene("Level 0");
	}

	public void PlayLevel(int level)
	{
		SceneManager.LoadScene($"Level {level}");
	}

	public void Exit()
	{
		Application.Quit();
	}
}