using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class WinScreen : Window
{
	[SerializeField] private TextMeshProUGUI level;
	[SerializeField] private TextMeshProUGUI time;

	public override void Open()
	{
		base.Open();
		float totalSeconds = GameManager.me.LevelTime();

		var ss = Convert.ToInt32(totalSeconds % 60).ToString("00");
		var mm = (Math.Floor(totalSeconds / 60) % 60).ToString("00");
		level.text = $"Level {GameManager.me.levelNumber} passed";
		time.text = $"{mm}:{ss}";
	}
}