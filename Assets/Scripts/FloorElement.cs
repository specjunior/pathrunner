using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorElement : MonoBehaviour
{
	[SerializeField, HideInInspector] private Floor floor;

	[FoldoutGroup("Materials"), SerializeField] private MeshRenderer meshRenderer;
	[FoldoutGroup("Materials"), SerializeField] private Material goodStep;
	[FoldoutGroup("Materials"), SerializeField] private Material badStep;

	[FoldoutGroup("Audio"), SerializeField] private AudioSource audioSource;
	[FoldoutGroup("Audio"), SerializeField] private AudioClip goodStepClip;
	[FoldoutGroup("Audio"), SerializeField] private AudioClip badStepClip;

	private Material baseMaterial;
	private bool indicated = false;

	private void Start()
	{
		baseMaterial = meshRenderer.material;
	}

	public void ShowPreview()
	{
		meshRenderer.material = goodStep;
	}

	public void GoodAnswer()
	{
		meshRenderer.material = goodStep;
		audioSource.PlayOneShot(goodStepClip);
	}

	public void BadAnswer()
	{
		meshRenderer.material = badStep;
		audioSource.PlayOneShot(badStepClip);
	}

	public void Restart()
	{
		meshRenderer.material = baseMaterial;
		indicated = false;
	}

	public void Configure(Floor floor_)
	{
		floor = floor_;
	}

	private void OnTriggerEnter(Collider other)
	{
		if(floor == null)
		{
			Debug.LogError("[FloorElement] Floor script not assigned", gameObject);
			return;
		}
		if(!indicated && other.transform.tag == "Player")
		{
			floor.PlayerEnteredFloorElement(this);
			indicated = true;
		}
	}
}